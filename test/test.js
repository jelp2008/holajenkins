var request = require('supertest');
var app = require('../app.js');
 
describe('GET /', function() {
  it('respond with hola mundo jenkins', function(done) {
    request(app).get('/').expect('hola mundo jenkins', done);
  });
});